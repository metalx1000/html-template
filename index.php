<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
    <div class="flex">
      <input id="input" type="text" placeholder="Eneter Text Here"/>
    </div>
    <div class="flex">
      <button class="button">Button #1</button>
      <button class="button-red">Button #2</button>
      <button class="button-blue">Button #3</button>
      <button class="button-green">Button #4</button>
    </div>
  </body>
  <script src="js/main.js"></script>
  <script>
    var input = document.querySelector("#input");
    var buttons = document.querySelector(".button");
  </script>
</html>

