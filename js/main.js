/*
@licstart  The following is the entire license notice for the
code in this page.

Copyright (C) 2022  Kris Occhipinti
https://filmsbykris.com

https://www.gnu.org/licenses/gpl-3.0.txt

The code in this page is free software: you can
redistribute it and/or modify it under the terms of the GNU
General Public License (GNU GPL) as published by the Free Software
Foundation, either version 3 of the License
The code is distributed WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.


@licend  The above is the entire license notice
for the JavaScript code in this page.
*/


function get(url, success) {
  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  xhr.open('GET', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.send();
  return xhr;
}

// example request
//get('http://foo.bar/?p1=1&p2=Hello+World', function(data){ console.log(data); });



function postAjax(url, data, success) {
  var params = typeof data == 'string' ? data : Object.keys(data).map(
    function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
  ).join('&');

  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
  xhr.open('POST', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send(params);
  return xhr;
}

// example request
//post('http://foo.bar/', 'p1=1&p2=Hello+World', function(data){ console.log(data); });

// example request with data object
//post('http://foo.bar/', { p1: 1, p2: 'Hello World' }, function(data){ console.log(data); });


//document.addEventListener('keyup',keyUp,false);
function keyUp(event){
  if(event.key == "ArrowUp"){
    console.log("Up Arrow Released");
  }else if(event.key == " "){
    console.log("Spacebar Released");
  }else if(event.key == "s"){
    console.log("Key 's' Released");
  }else{
    console.log(event.key + " Released");
  }
}


//document.addEventListener('keydown',keyDown,false);
function keyDown(event){
  if(event.key == "ArrowUp"){
    console.log("Up Arrow");
  }else if(event.key == " "){
    console.log("Spacebar");
  }else if(event.key == "s"){
    console.log("Key 's'");
  }else{
    console.log(event.key);
  }
}


function submit(success){
  var form = document.querySelector('form');
  var params = new URLSearchParams(new FormData(form)).toString();
  var url = form.action + "?" + params;
  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
  xhr.open('GET', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.send();
  return xhr;
}


function randomString(length) {
  if ( typeof length === 'undefined'){length = 10};
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

